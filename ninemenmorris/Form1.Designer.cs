﻿namespace ninemenmorris
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SET_A = new System.Windows.Forms.Panel();
            this.duck_A3 = new System.Windows.Forms.Button();
            this.duck_A4 = new System.Windows.Forms.Button();
            this.duck_A2 = new System.Windows.Forms.Button();
            this.duck_A5 = new System.Windows.Forms.Button();
            this.duck_A9 = new System.Windows.Forms.Button();
            this.duck_A8 = new System.Windows.Forms.Button();
            this.duck_A7 = new System.Windows.Forms.Button();
            this.duck_A6 = new System.Windows.Forms.Button();
            this.duck_A1 = new System.Windows.Forms.Button();
            this.SET_B = new System.Windows.Forms.Panel();
            this.duck_B9 = new System.Windows.Forms.Button();
            this.duck_B8 = new System.Windows.Forms.Button();
            this.duck_B7 = new System.Windows.Forms.Button();
            this.duck_B6 = new System.Windows.Forms.Button();
            this.duck_B5 = new System.Windows.Forms.Button();
            this.duck_B4 = new System.Windows.Forms.Button();
            this.duck_B3 = new System.Windows.Forms.Button();
            this.duck_B2 = new System.Windows.Forms.Button();
            this.duck_B1 = new System.Windows.Forms.Button();
            this.Board = new System.Windows.Forms.Panel();
            this.pos23 = new System.Windows.Forms.Button();
            this.pos21 = new System.Windows.Forms.Button();
            this.pos24 = new System.Windows.Forms.Button();
            this.pos22 = new System.Windows.Forms.Button();
            this.pos17 = new System.Windows.Forms.Button();
            this.pos20 = new System.Windows.Forms.Button();
            this.pos18 = new System.Windows.Forms.Button();
            this.pos19 = new System.Windows.Forms.Button();
            this.pos15 = new System.Windows.Forms.Button();
            this.pos16 = new System.Windows.Forms.Button();
            this.pos14 = new System.Windows.Forms.Button();
            this.pos13 = new System.Windows.Forms.Button();
            this.pos12 = new System.Windows.Forms.Button();
            this.pos7 = new System.Windows.Forms.Button();
            this.pos8 = new System.Windows.Forms.Button();
            this.pos9 = new System.Windows.Forms.Button();
            this.pos10 = new System.Windows.Forms.Button();
            this.pos11 = new System.Windows.Forms.Button();
            this.pos3 = new System.Windows.Forms.Button();
            this.pos4 = new System.Windows.Forms.Button();
            this.pos2 = new System.Windows.Forms.Button();
            this.pos5 = new System.Windows.Forms.Button();
            this.pos6 = new System.Windows.Forms.Button();
            this.pos1 = new System.Windows.Forms.Button();
            this.restart_button = new System.Windows.Forms.Button();
            this.exit_button = new System.Windows.Forms.Button();
            this.Start = new System.Windows.Forms.Button();
            this.Message = new System.Windows.Forms.Label();
            this.SET_A.SuspendLayout();
            this.SET_B.SuspendLayout();
            this.Board.SuspendLayout();
            this.SuspendLayout();
            // 
            // SET_A
            // 
            this.SET_A.Controls.Add(this.duck_A3);
            this.SET_A.Controls.Add(this.duck_A4);
            this.SET_A.Controls.Add(this.duck_A2);
            this.SET_A.Controls.Add(this.duck_A5);
            this.SET_A.Controls.Add(this.duck_A9);
            this.SET_A.Controls.Add(this.duck_A8);
            this.SET_A.Controls.Add(this.duck_A7);
            this.SET_A.Controls.Add(this.duck_A6);
            this.SET_A.Controls.Add(this.duck_A1);
            this.SET_A.Location = new System.Drawing.Point(582, 100);
            this.SET_A.Name = "SET_A";
            this.SET_A.Size = new System.Drawing.Size(300, 49);
            this.SET_A.TabIndex = 1;
            // 
            // duck_A3
            // 
            this.duck_A3.BackgroundImage = global::ninemenmorris.Properties.Resources.blue_bird__1_;
            this.duck_A3.Location = new System.Drawing.Point(58, 12);
            this.duck_A3.Name = "duck_A3";
            this.duck_A3.Size = new System.Drawing.Size(26, 25);
            this.duck_A3.TabIndex = 8;
            this.duck_A3.UseVisualStyleBackColor = true;
            this.duck_A3.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_A4
            // 
            this.duck_A4.BackgroundImage = global::ninemenmorris.Properties.Resources.blue_bird__1_;
            this.duck_A4.Location = new System.Drawing.Point(88, 12);
            this.duck_A4.Name = "duck_A4";
            this.duck_A4.Size = new System.Drawing.Size(26, 25);
            this.duck_A4.TabIndex = 7;
            this.duck_A4.UseVisualStyleBackColor = true;
            this.duck_A4.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_A2
            // 
            this.duck_A2.BackgroundImage = global::ninemenmorris.Properties.Resources.blue_bird__1_;
            this.duck_A2.Location = new System.Drawing.Point(34, 12);
            this.duck_A2.Name = "duck_A2";
            this.duck_A2.Size = new System.Drawing.Size(26, 25);
            this.duck_A2.TabIndex = 6;
            this.duck_A2.UseVisualStyleBackColor = true;
            this.duck_A2.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_A5
            // 
            this.duck_A5.BackgroundImage = global::ninemenmorris.Properties.Resources.blue_bird__1_;
            this.duck_A5.Location = new System.Drawing.Point(116, 12);
            this.duck_A5.Name = "duck_A5";
            this.duck_A5.Size = new System.Drawing.Size(26, 25);
            this.duck_A5.TabIndex = 5;
            this.duck_A5.UseVisualStyleBackColor = true;
            this.duck_A5.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_A9
            // 
            this.duck_A9.BackgroundImage = global::ninemenmorris.Properties.Resources.blue_bird__1_;
            this.duck_A9.Location = new System.Drawing.Point(231, 12);
            this.duck_A9.Name = "duck_A9";
            this.duck_A9.Size = new System.Drawing.Size(26, 25);
            this.duck_A9.TabIndex = 4;
            this.duck_A9.UseVisualStyleBackColor = true;
            this.duck_A9.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_A8
            // 
            this.duck_A8.BackgroundImage = global::ninemenmorris.Properties.Resources.blue_bird__1_;
            this.duck_A8.Location = new System.Drawing.Point(200, 12);
            this.duck_A8.Name = "duck_A8";
            this.duck_A8.Size = new System.Drawing.Size(26, 25);
            this.duck_A8.TabIndex = 3;
            this.duck_A8.UseVisualStyleBackColor = true;
            this.duck_A8.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_A7
            // 
            this.duck_A7.BackgroundImage = global::ninemenmorris.Properties.Resources.blue_bird__1_;
            this.duck_A7.Location = new System.Drawing.Point(170, 12);
            this.duck_A7.Name = "duck_A7";
            this.duck_A7.Size = new System.Drawing.Size(26, 25);
            this.duck_A7.TabIndex = 2;
            this.duck_A7.UseVisualStyleBackColor = true;
            this.duck_A7.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_A6
            // 
            this.duck_A6.BackgroundImage = global::ninemenmorris.Properties.Resources.blue_bird__1_;
            this.duck_A6.Location = new System.Drawing.Point(138, 12);
            this.duck_A6.Name = "duck_A6";
            this.duck_A6.Size = new System.Drawing.Size(26, 25);
            this.duck_A6.TabIndex = 1;
            this.duck_A6.UseVisualStyleBackColor = true;
            this.duck_A6.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_A1
            // 
            this.duck_A1.BackgroundImage = global::ninemenmorris.Properties.Resources.blue_bird__1_;
            this.duck_A1.Location = new System.Drawing.Point(3, 12);
            this.duck_A1.Name = "duck_A1";
            this.duck_A1.Size = new System.Drawing.Size(26, 25);
            this.duck_A1.TabIndex = 0;
            this.duck_A1.UseVisualStyleBackColor = true;
            this.duck_A1.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // SET_B
            // 
            this.SET_B.Controls.Add(this.duck_B9);
            this.SET_B.Controls.Add(this.duck_B8);
            this.SET_B.Controls.Add(this.duck_B7);
            this.SET_B.Controls.Add(this.duck_B6);
            this.SET_B.Controls.Add(this.duck_B5);
            this.SET_B.Controls.Add(this.duck_B4);
            this.SET_B.Controls.Add(this.duck_B3);
            this.SET_B.Controls.Add(this.duck_B2);
            this.SET_B.Controls.Add(this.duck_B1);
            this.SET_B.Location = new System.Drawing.Point(576, 422);
            this.SET_B.Name = "SET_B";
            this.SET_B.Size = new System.Drawing.Size(300, 49);
            this.SET_B.TabIndex = 2;
            // 
            // duck_B9
            // 
            this.duck_B9.BackgroundImage = global::ninemenmorris.Properties.Resources.yellow_duck;
            this.duck_B9.Location = new System.Drawing.Point(262, 12);
            this.duck_B9.Name = "duck_B9";
            this.duck_B9.Size = new System.Drawing.Size(26, 25);
            this.duck_B9.TabIndex = 11;
            this.duck_B9.UseVisualStyleBackColor = true;
            this.duck_B9.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_B8
            // 
            this.duck_B8.BackgroundImage = global::ninemenmorris.Properties.Resources.yellow_duck;
            this.duck_B8.Location = new System.Drawing.Point(231, 12);
            this.duck_B8.Name = "duck_B8";
            this.duck_B8.Size = new System.Drawing.Size(26, 25);
            this.duck_B8.TabIndex = 10;
            this.duck_B8.UseVisualStyleBackColor = true;
            this.duck_B8.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_B7
            // 
            this.duck_B7.BackgroundImage = global::ninemenmorris.Properties.Resources.yellow_duck;
            this.duck_B7.Location = new System.Drawing.Point(200, 12);
            this.duck_B7.Name = "duck_B7";
            this.duck_B7.Size = new System.Drawing.Size(26, 25);
            this.duck_B7.TabIndex = 9;
            this.duck_B7.UseVisualStyleBackColor = true;
            this.duck_B7.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_B6
            // 
            this.duck_B6.BackgroundImage = global::ninemenmorris.Properties.Resources.yellow_duck;
            this.duck_B6.Location = new System.Drawing.Point(170, 12);
            this.duck_B6.Name = "duck_B6";
            this.duck_B6.Size = new System.Drawing.Size(26, 25);
            this.duck_B6.TabIndex = 8;
            this.duck_B6.UseVisualStyleBackColor = true;
            this.duck_B6.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_B5
            // 
            this.duck_B5.BackgroundImage = global::ninemenmorris.Properties.Resources.yellow_duck;
            this.duck_B5.Location = new System.Drawing.Point(138, 12);
            this.duck_B5.Name = "duck_B5";
            this.duck_B5.Size = new System.Drawing.Size(26, 25);
            this.duck_B5.TabIndex = 7;
            this.duck_B5.UseVisualStyleBackColor = true;
            this.duck_B5.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_B4
            // 
            this.duck_B4.BackgroundImage = global::ninemenmorris.Properties.Resources.yellow_duck;
            this.duck_B4.Location = new System.Drawing.Point(96, 12);
            this.duck_B4.Name = "duck_B4";
            this.duck_B4.Size = new System.Drawing.Size(26, 25);
            this.duck_B4.TabIndex = 3;
            this.duck_B4.UseVisualStyleBackColor = true;
            this.duck_B4.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_B3
            // 
            this.duck_B3.BackgroundImage = global::ninemenmorris.Properties.Resources.yellow_duck;
            this.duck_B3.Location = new System.Drawing.Point(64, 12);
            this.duck_B3.Name = "duck_B3";
            this.duck_B3.Size = new System.Drawing.Size(26, 25);
            this.duck_B3.TabIndex = 4;
            this.duck_B3.UseVisualStyleBackColor = true;
            this.duck_B3.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_B2
            // 
            this.duck_B2.BackgroundImage = global::ninemenmorris.Properties.Resources.yellow_duck;
            this.duck_B2.Location = new System.Drawing.Point(34, 12);
            this.duck_B2.Name = "duck_B2";
            this.duck_B2.Size = new System.Drawing.Size(26, 25);
            this.duck_B2.TabIndex = 5;
            this.duck_B2.UseVisualStyleBackColor = true;
            this.duck_B2.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // duck_B1
            // 
            this.duck_B1.BackgroundImage = global::ninemenmorris.Properties.Resources.yellow_duck;
            this.duck_B1.Location = new System.Drawing.Point(3, 12);
            this.duck_B1.Name = "duck_B1";
            this.duck_B1.Size = new System.Drawing.Size(26, 25);
            this.duck_B1.TabIndex = 6;
            this.duck_B1.UseVisualStyleBackColor = true;
            this.duck_B1.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // Board
            // 
            this.Board.BackColor = System.Drawing.Color.Cyan;
            this.Board.BackgroundImage = global::ninemenmorris.Properties.Resources.smallboard;
            this.Board.Controls.Add(this.pos23);
            this.Board.Controls.Add(this.pos21);
            this.Board.Controls.Add(this.pos24);
            this.Board.Controls.Add(this.pos22);
            this.Board.Controls.Add(this.pos17);
            this.Board.Controls.Add(this.pos20);
            this.Board.Controls.Add(this.pos18);
            this.Board.Controls.Add(this.pos19);
            this.Board.Controls.Add(this.pos15);
            this.Board.Controls.Add(this.pos16);
            this.Board.Controls.Add(this.pos14);
            this.Board.Controls.Add(this.pos13);
            this.Board.Controls.Add(this.pos12);
            this.Board.Controls.Add(this.pos7);
            this.Board.Controls.Add(this.pos8);
            this.Board.Controls.Add(this.pos9);
            this.Board.Controls.Add(this.pos10);
            this.Board.Controls.Add(this.pos11);
            this.Board.Controls.Add(this.pos3);
            this.Board.Controls.Add(this.pos4);
            this.Board.Controls.Add(this.pos2);
            this.Board.Controls.Add(this.pos5);
            this.Board.Controls.Add(this.pos6);
            this.Board.Controls.Add(this.pos1);
            this.Board.Location = new System.Drawing.Point(100, 100);
            this.Board.Name = "Board";
            this.Board.Size = new System.Drawing.Size(450, 462);
            this.Board.TabIndex = 0;
            // 
            // pos23
            // 
            this.pos23.Location = new System.Drawing.Point(164, 277);
            this.pos23.Name = "pos23";
            this.pos23.Size = new System.Drawing.Size(20, 20);
            this.pos23.TabIndex = 4;
            this.pos23.UseVisualStyleBackColor = true;
            this.pos23.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos21
            // 
            this.pos21.Location = new System.Drawing.Point(272, 277);
            this.pos21.Name = "pos21";
            this.pos21.Size = new System.Drawing.Size(20, 20);
            this.pos21.TabIndex = 3;
            this.pos21.UseVisualStyleBackColor = true;
            this.pos21.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos24
            // 
            this.pos24.Location = new System.Drawing.Point(164, 215);
            this.pos24.Name = "pos24";
            this.pos24.Size = new System.Drawing.Size(20, 20);
            this.pos24.TabIndex = 5;
            this.pos24.UseVisualStyleBackColor = true;
            this.pos24.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos22
            // 
            this.pos22.Location = new System.Drawing.Point(220, 277);
            this.pos22.Name = "pos22";
            this.pos22.Size = new System.Drawing.Size(20, 20);
            this.pos22.TabIndex = 10;
            this.pos22.UseVisualStyleBackColor = true;
            this.pos22.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos17
            // 
            this.pos17.Location = new System.Drawing.Point(272, 165);
            this.pos17.Name = "pos17";
            this.pos17.Size = new System.Drawing.Size(20, 20);
            this.pos17.TabIndex = 2;
            this.pos17.UseVisualStyleBackColor = true;
            this.pos17.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos20
            // 
            this.pos20.Location = new System.Drawing.Point(164, 165);
            this.pos20.Name = "pos20";
            this.pos20.Size = new System.Drawing.Size(20, 20);
            this.pos20.TabIndex = 2;
            this.pos20.UseVisualStyleBackColor = true;
            this.pos20.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos18
            // 
            this.pos18.Location = new System.Drawing.Point(220, 165);
            this.pos18.Name = "pos18";
            this.pos18.Size = new System.Drawing.Size(20, 20);
            this.pos18.TabIndex = 3;
            this.pos18.UseVisualStyleBackColor = true;
            this.pos18.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos19
            // 
            this.pos19.Location = new System.Drawing.Point(272, 215);
            this.pos19.Name = "pos19";
            this.pos19.Size = new System.Drawing.Size(20, 20);
            this.pos19.TabIndex = 4;
            this.pos19.UseVisualStyleBackColor = true;
            this.pos19.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos15
            // 
            this.pos15.Location = new System.Drawing.Point(100, 334);
            this.pos15.Name = "pos15";
            this.pos15.Size = new System.Drawing.Size(20, 20);
            this.pos15.TabIndex = 2;
            this.pos15.UseVisualStyleBackColor = true;
            this.pos15.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos16
            // 
            this.pos16.Location = new System.Drawing.Point(100, 215);
            this.pos16.Name = "pos16";
            this.pos16.Size = new System.Drawing.Size(20, 20);
            this.pos16.TabIndex = 3;
            this.pos16.UseVisualStyleBackColor = true;
            this.pos16.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos14
            // 
            this.pos14.Location = new System.Drawing.Point(220, 334);
            this.pos14.Name = "pos14";
            this.pos14.Size = new System.Drawing.Size(20, 20);
            this.pos14.TabIndex = 9;
            this.pos14.UseVisualStyleBackColor = true;
            this.pos14.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos13
            // 
            this.pos13.Location = new System.Drawing.Point(332, 334);
            this.pos13.Name = "pos13";
            this.pos13.Size = new System.Drawing.Size(20, 20);
            this.pos13.TabIndex = 8;
            this.pos13.UseVisualStyleBackColor = true;
            this.pos13.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos12
            // 
            this.pos12.Location = new System.Drawing.Point(332, 215);
            this.pos12.Name = "pos12";
            this.pos12.Size = new System.Drawing.Size(20, 20);
            this.pos12.TabIndex = 7;
            this.pos12.UseVisualStyleBackColor = true;
            this.pos12.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos7
            // 
            this.pos7.Location = new System.Drawing.Point(48, 397);
            this.pos7.Name = "pos7";
            this.pos7.Size = new System.Drawing.Size(20, 20);
            this.pos7.TabIndex = 2;
            this.pos7.UseVisualStyleBackColor = true;
            this.pos7.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos8
            // 
            this.pos8.Location = new System.Drawing.Point(48, 215);
            this.pos8.Name = "pos8";
            this.pos8.Size = new System.Drawing.Size(20, 20);
            this.pos8.TabIndex = 3;
            this.pos8.UseVisualStyleBackColor = true;
            this.pos8.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos9
            // 
            this.pos9.Location = new System.Drawing.Point(100, 100);
            this.pos9.Name = "pos9";
            this.pos9.Size = new System.Drawing.Size(20, 20);
            this.pos9.TabIndex = 4;
            this.pos9.UseVisualStyleBackColor = true;
            this.pos9.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos10
            // 
            this.pos10.Location = new System.Drawing.Point(220, 100);
            this.pos10.Name = "pos10";
            this.pos10.Size = new System.Drawing.Size(20, 20);
            this.pos10.TabIndex = 5;
            this.pos10.UseVisualStyleBackColor = true;
            this.pos10.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos11
            // 
            this.pos11.Location = new System.Drawing.Point(332, 100);
            this.pos11.Name = "pos11";
            this.pos11.Size = new System.Drawing.Size(20, 20);
            this.pos11.TabIndex = 6;
            this.pos11.UseVisualStyleBackColor = true;
            this.pos11.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos3
            // 
            this.pos3.Location = new System.Drawing.Point(386, 49);
            this.pos3.Name = "pos3";
            this.pos3.Size = new System.Drawing.Size(20, 20);
            this.pos3.TabIndex = 3;
            this.pos3.UseVisualStyleBackColor = true;
            this.pos3.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos4
            // 
            this.pos4.Location = new System.Drawing.Point(386, 215);
            this.pos4.Name = "pos4";
            this.pos4.Size = new System.Drawing.Size(20, 20);
            this.pos4.TabIndex = 4;
            this.pos4.UseVisualStyleBackColor = true;
            this.pos4.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos2
            // 
            this.pos2.Location = new System.Drawing.Point(220, 45);
            this.pos2.Name = "pos2";
            this.pos2.Size = new System.Drawing.Size(20, 20);
            this.pos2.TabIndex = 2;
            this.pos2.UseVisualStyleBackColor = true;
            this.pos2.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos5
            // 
            this.pos5.Location = new System.Drawing.Point(386, 397);
            this.pos5.Name = "pos5";
            this.pos5.Size = new System.Drawing.Size(20, 20);
            this.pos5.TabIndex = 5;
            this.pos5.UseVisualStyleBackColor = true;
            this.pos5.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos6
            // 
            this.pos6.Location = new System.Drawing.Point(220, 397);
            this.pos6.Name = "pos6";
            this.pos6.Size = new System.Drawing.Size(20, 20);
            this.pos6.TabIndex = 6;
            this.pos6.UseVisualStyleBackColor = true;
            this.pos6.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // pos1
            // 
            this.pos1.Location = new System.Drawing.Point(48, 49);
            this.pos1.Name = "pos1";
            this.pos1.Size = new System.Drawing.Size(20, 20);
            this.pos1.TabIndex = 1;
            this.pos1.UseVisualStyleBackColor = true;
            this.pos1.Click += new System.EventHandler(this.moveDuck_Click);
            // 
            // restart_button
            // 
            this.restart_button.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.restart_button.Location = new System.Drawing.Point(548, 497);
            this.restart_button.Name = "restart_button";
            this.restart_button.Size = new System.Drawing.Size(200, 49);
            this.restart_button.TabIndex = 3;
            this.restart_button.Text = "RESTART";
            this.restart_button.UseVisualStyleBackColor = false;
            this.restart_button.Click += new System.EventHandler(this.restart_button_Click_1);
            // 
            // exit_button
            // 
            this.exit_button.BackColor = System.Drawing.Color.IndianRed;
            this.exit_button.Location = new System.Drawing.Point(264, 523);
            this.exit_button.Name = "exit_button";
            this.exit_button.Size = new System.Drawing.Size(100, 49);
            this.exit_button.TabIndex = 4;
            this.exit_button.Text = "EXIT";
            this.exit_button.UseVisualStyleBackColor = false;
            this.exit_button.Click += new System.EventHandler(this.exit_button_Click_1);
            // 
            // Start
            // 
            this.Start.BackColor = System.Drawing.Color.GreenYellow;
            this.Start.Location = new System.Drawing.Point(610, 561);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(100, 50);
            this.Start.TabIndex = 5;
            this.Start.Text = "START";
            this.Start.UseVisualStyleBackColor = false;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // Message
            // 
            this.Message.AutoSize = true;
            this.Message.BackColor = System.Drawing.Color.SandyBrown;
            this.Message.Location = new System.Drawing.Point(649, 265);
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(47, 20);
            this.Message.TabIndex = 6;
            this.Message.Text = "INFO";
            this.Message.Click += new System.EventHandler(this.Message_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 651);
            this.Controls.Add(this.Message);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.exit_button);
            this.Controls.Add(this.restart_button);
            this.Controls.Add(this.SET_B);
            this.Controls.Add(this.SET_A);
            this.Controls.Add(this.Board);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SET_A.ResumeLayout(false);
            this.SET_B.ResumeLayout(false);
            this.Board.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Board;
        private System.Windows.Forms.Button pos1;
        private System.Windows.Forms.Button pos15;
        private System.Windows.Forms.Button pos16;
        private System.Windows.Forms.Button pos14;
        private System.Windows.Forms.Button pos13;
        private System.Windows.Forms.Button pos12;
        private System.Windows.Forms.Button pos7;
        private System.Windows.Forms.Button pos8;
        private System.Windows.Forms.Button pos9;
        private System.Windows.Forms.Button pos10;
        private System.Windows.Forms.Button pos11;
        private System.Windows.Forms.Button pos3;
        private System.Windows.Forms.Button pos4;
        private System.Windows.Forms.Button pos2;
        private System.Windows.Forms.Button pos5;
        private System.Windows.Forms.Button pos6;
        private System.Windows.Forms.Button pos22;
        private System.Windows.Forms.Button pos17;
        private System.Windows.Forms.Button pos20;
        private System.Windows.Forms.Button pos18;
        private System.Windows.Forms.Button pos19;
        private System.Windows.Forms.Button pos21;
        private System.Windows.Forms.Button pos23;
        private System.Windows.Forms.Button pos24;
        private System.Windows.Forms.Panel SET_A;
        private System.Windows.Forms.Button duck_A1;
        private System.Windows.Forms.Panel SET_B;
        private System.Windows.Forms.Button duck_A3;
        private System.Windows.Forms.Button duck_A4;
        private System.Windows.Forms.Button duck_A2;
        private System.Windows.Forms.Button duck_A5;
        private System.Windows.Forms.Button duck_A9;
        private System.Windows.Forms.Button duck_A8;
        private System.Windows.Forms.Button duck_A7;
        private System.Windows.Forms.Button duck_A6;
        private System.Windows.Forms.Button duck_B4;
        private System.Windows.Forms.Button duck_B3;
        private System.Windows.Forms.Button duck_B2;
        private System.Windows.Forms.Button duck_B1;
        private System.Windows.Forms.Button duck_B9;
        private System.Windows.Forms.Button duck_B8;
        private System.Windows.Forms.Button duck_B7;
        private System.Windows.Forms.Button duck_B6;
        private System.Windows.Forms.Button duck_B5;
        private System.Windows.Forms.Button restart_button;
        private System.Windows.Forms.Button exit_button;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.Label Message;
    }
}

