﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ninemenmorris
{
    public partial class Form1 : Form
    {
        //arrays to store ducks positions 
        int[] APositions;
        int[] BPositions;

        int placed = 0;

        char currentPlayer = 'A';

        string imagePath = System.Windows.Forms.Application.StartupPath + "..\\..\\..\\Images\\";


        //variables to store eliminated ducks 
        int ARemoved;
        int BRemoved;

        //checking whether allignment happened 
        bool AAligned = false;
        bool BAligned = false;

        Button[] possiblePos;
        Button[] ADucks;
        Button[] BDucks;

        bool firstClick = true;
        Button clickedPos;

        public Form1()
        {
            InitializeComponent();
        }

        //
        public void moveDuck_Click(object sender, EventArgs e)
        {
            Button duck = sender as Button;
            //placement phase
            if (placed < 19 && duck.Image == null)
            {
                duck_A1.Image = null;
                PlaceDuck(duck);
                Message.Text = "PLAYER -" + currentPlayer + "TURN";
            }
            //movement phase 
            else
            {
                if (firstClick)
                {
                    if (currentPlayer == 'A' && ADucks.Contains(duck))
                    {
                        clickedPos = duck;
                        firstClick = false;
                    }
                    else if (currentPlayer == 'B' && BDucks.Contains(duck))
                    {
                        clickedPos = duck;
                        firstClick = false;
                    }
                }
                else
                {
                    if (possiblePos.Contains(duck) && duck.Image == null)
                    {
                        MoveDuck(clickedPos, duck);

                        if (currentPlayer == 'A')
                        {
                            currentPlayer = 'B';
                        }
                        else
                        {
                            currentPlayer = 'A';
                        }
                        firstClick = false;
                    }

                }
            }
        }

        //placement phase 
        private void PlaceDuck(Button duck)
        {
            placed += 1;
            if (currentPlayer == 'A')
            {
                duck.Image = System.Drawing.Image.FromFile(imagePath + "blue_duck.png");
                currentPlayer = 'B';
            }
            else
            {
                duck.Image = System.Drawing.Image.FromFile(imagePath + "yellow_duck.png");
                currentPlayer = 'A';
            }

        }

        private void MoveDuck(Button initial, Button transfer)
        {
            int fromIndex = -1;

            if (placed < 19)
            {
                if (currentPlayer == 'A')
                {
                    transfer.Image = System.Drawing.Image.FromFile(imagePath + "blue_duck.png");
                    initial.Image = null;
                    placed += 1;

                    fromIndex = Array.IndexOf(ADucks, initial);
                }
                else if (currentPlayer == 'B')
                {
                    transfer.Image = System.Drawing.Image.FromFile(imagePath + "yellow_duck.png");
                    initial.Image = null;
                    placed += 1;

                    fromIndex = Array.IndexOf(BDucks, initial);
                }


                int toIndex = Array.IndexOf(possiblePos, transfer);

                if (currentPlayer == 'A' && fromIndex != -1)
                {
                    APositions[fromIndex] = 1;
                }
                else if (currentPlayer == 'B' && fromIndex != -1)
                {
                    BPositions[fromIndex] = 1;
                }
            }
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            string AImagePath = imagePath + "blue_duck.png";
            string BImagePath = imagePath + "yellow_duck.png";

            possiblePos = new Button[] { pos1 ,pos2 ,pos3 ,pos4, pos5, pos6, pos7 ,pos8,
                                         pos9 ,pos10 , pos11,pos12 ,pos13 ,pos14 ,pos15,
                                         pos16 ,pos17 ,pos18 ,pos19 ,pos20 ,pos21 ,pos22,
                                         pos23 , pos24};

           // ADucks = new Button[] {duck_A1,duck_A2,duck_A3,duck_A4,duck_A5,
                                   // duck_A6,duck_A7,duck_A8,duck_A9};

           // BDucks = new Button[] {duck_B1,duck_B2,duck_B3,duck_B4,duck_B5,
                                  // duck_B6,duck_B7,duck_B8,duck_B9 };

            //initialize everything

            resetgame();

        }

        private void resetgame()
        {
            AAligned = false;
            BAligned = false;

            ARemoved = 0;
            BRemoved = 0;

            //removing objects 
            foreach (Button pos in possiblePos)
            {
                pos.Image = null;
            }

            //Initialising the positions outside the board
            APositions = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            BPositions = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            currentPlayer = 'A';

        }

        private void exit_button_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void restart_button_Click_1(object sender, EventArgs e)
        {
            resetgame();
        }

        private void Start_Click(object sender, EventArgs e)
        {
            Message.Text = "PLAYER -" + currentPlayer + "TURN";
        }

        private void Message_Click(object sender, EventArgs e)
        {

        }
    }
}




