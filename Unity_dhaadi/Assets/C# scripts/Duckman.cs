using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duckman : MonoBehaviour
{
    public GameObject controller;
    public GameObject movePlate;

    private int xBoard = -1;
    private int yBoard = -1;

    // Variable to keep track of player A or B
    public string player;

    // References for all the sprites
    public Sprite yellow_duck;
    public Sprite white_duck;

    public void Activate()
    {
        controller = GameObject.FindGameObjectWithTag("GameController");
        //
        if (controller == null)
        {
            Debug.LogError("GameController not found! Make sure the GameController is tagged correctly.");
            return;
        }
        //
        SetCoords();

        switch (this.name)
        {
            case "yellow_duck":
                this.GetComponent<SpriteRenderer>().sprite = yellow_duck;
                player = "A";
                break;
            case "white_duck":
                this.GetComponent<SpriteRenderer>().sprite = white_duck;
                player = "B";
                break;
                //
            default:
                Debug.LogError("Duckman name does not match any known players.");
                break;
                //
        }

    }

    public void SetCoords()
    {
        if (xBoard == -1 && yBoard == -1) 
        {return;} //out of board
        float xPos = xBoard * 0.66f - 2.3f;
        float yPos = yBoard * 0.66f - 2.3f;
        this.transform.position = new Vector3(xPos, yPos, -1.0f);
    }

    public int GetXBoard()
    {
        return xBoard;
    }

    public int GetYBoard()
    {
        return yBoard;
    }

    public void SetXBoard(int x)
    {
        xBoard = x;
    }

    public void SetYBoard(int y)
    {
        yBoard = y;
    }

    public void SetBoardPosition(int x, int y)
    {
        xBoard = x;
        yBoard = y;
    }

    private void OnMouseUp()
    {
        //
        if (controller == null)
        {
            Debug.LogError("Controller reference is null in Duckman.OnMouseUp!");
            return;
        }

        Game game = controller.GetComponent<Game>();

        if (game == null)
        {
            Debug.LogError("Game component not found on the controller!");
            return;
        }
        //
       

        if (!game.GameOver() && game.GetCurrentPlayer() == player)
        {
            if (xBoard == -1 && yBoard == -1)
            {
                // Placement phase
                game.DestroyMovePlates();
                game.InitiateMovePlates();
            }
            else
            {
                // Movement phase
                game.DestroyMovePlates();
                InitiateMovePlates();
            }
        }
    }

    public void InitiateMovePlates()
    {
        Game game = controller.GetComponent<Game>();

            // Movement phase
            MovePlateSpawn(xBoard, yBoard + 1);
            MovePlateSpawn(xBoard, yBoard - 1);
            MovePlateSpawn(xBoard + 1, yBoard);
            MovePlateSpawn(xBoard - 1, yBoard);
    }

    public void MovePlateSpawn(int matrixX, int matrixY)
    {
        Game game = controller.GetComponent<Game>();

        if (game.PositionOnBoard(matrixX, matrixY) && game.GetPosition(matrixX, matrixY) == null)
        {
            GameObject mp = Instantiate(movePlate, new Vector3(0, 0, -1), Quaternion.identity);

            MovePlate mpScript = mp.GetComponent<MovePlate>();
            mpScript.SetReference(gameObject);
            mpScript.SetCoords(matrixX, matrixY);
        }
    }
}
