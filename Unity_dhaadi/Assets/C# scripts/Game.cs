using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public GameObject playerpiece;
    public GameObject movePlatePrefab;
    public GameObject controller;
    
    private GameObject[,] positions = new GameObject[9, 9];
    public GameObject[] playerA = new GameObject[9];
    public GameObject[] playerB = new GameObject[9];

    private string currentPlayer = "A";
    private bool gameOver = false;
    public int placedPiecesA = 0;
    public int placedPiecesB = 0;

/*
    void Start()
    {
        playerA = new GameObject[] {
            Create("white_duck", -2, -2),
            Create("white_duck", -2, -2),
            Create("white_duck", -2, -2),
            Create("white_duck", -2, -),
            Create("white_duck", -1, -1),
            Create("white_duck", -1, -1),
            Create("white_duck", -1, -1),
            Create("white_duck", -1, -1),
            Create("white_duck", -1, -1)
        };

        playerB = new GameObject[] {
            Create("yellow_duck", -1, -1),
            Create("yellow_duck", -1, -1),
            Create("yellow_duck", -1, -1),
            Create("yellow_duck", -1, -1),
            Create("yellow_duck", -1, -1),
            Create("yellow_duck", -1, -1),
            Create("yellow_duck", -1, -1),
            Create("yellow_duck", -1, -1),
            Create("yellow_duck", -1, -1)
        };
    }
*/
    void Start()
    {
    Vector3 whiteDuckStartPos = new Vector3(-5, 2, -1);
    Vector3 yellowDuckStartPos = new Vector3(5, -2, -1); 
    float offset = 0.5f; // avoid overlapping

    // Create and place white ducks
    for (int i = 0; i < playerA.Length; i++)
    {
        playerA[i] = Create("white_duck", -1, -1);
        playerA[i].transform.position = whiteDuckStartPos - new Vector3(0, i*offset, 0); 
    }

    // Create and place yellow ducks
    for (int i = 0; i < playerB.Length; i++)
    {
        playerB[i] = Create("yellow_duck", -1, -1);
        playerB[i].transform.position = yellowDuckStartPos + new Vector3(0, i * offset, 0); 
    }
    }
    // Create a new piece and place it on the screen
    public GameObject Create(string name, int x, int y)
    {
        GameObject obj = Instantiate(playerpiece, new Vector3(0, 0, -1), Quaternion.identity);
        if (name == "yellow_duck")
        {
            obj.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        }
        else {
            obj.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }
        Duckman dm = obj.GetComponent<Duckman>();
        dm.name = name;
        dm.SetBoardPosition(x, y);
        dm.Activate();
        return obj;
    }

    // Set position of a piece on the board
    public void SetPosition(GameObject obj)
    {
        Duckman dm = obj.GetComponent<Duckman>();
        int x = dm.GetXBoard();
        int y = dm.GetYBoard();
        positions[x, y] = obj;
    }

    public void SetPositionEmpty(int x, int y)
    {
        positions[x, y] = null;
    }

    public GameObject GetPosition(int x, int y)
    {
        return positions[x, y];
    }

    public bool PositionOnBoard(int x, int y)
    {
        if (x < 0 || y < 0 || x >= 7|| y >= 7)
        {
            return false;
        }
        return true;
    }

    public void DestroyMovePlates()
    {
        GameObject[] movePlates = GameObject.FindGameObjectsWithTag("MovePlate");
        foreach (GameObject mp in movePlates)
        {
            Destroy(mp);
        }
    }

    public void CheckMill(int x, int y, string player)
    {
        if (CheckLine(x, y, player))
        {
            // remove opponent duck
            Debug.Log(player + " formed a mill! Remove an opponent's duck.");
        }
    }

    private bool CheckLine(int x, int y, string player)
    {
        // Check horizontal line
        if (x >= 0 && x <= 6 && positions[x, y] != null && positions[x + 1, y] != null && positions[x + 2, y] != null)
        {
            if (positions[x, y].GetComponent<Duckman>().player == player &&
                positions[x + 1, y].GetComponent<Duckman>().player == player &&
                positions[x + 2, y].GetComponent<Duckman>().player == player)
            {
                return true;
            }
        }

        // Check vertical line
        if (y >= 0 && y <= 6 && positions[x, y] != null && positions[x, y + 1] != null && positions[x, y + 2] != null)
        {
            if (positions[x, y].GetComponent<Duckman>().player == player &&
                positions[x, y + 1].GetComponent<Duckman>().player == player &&
                positions[x, y + 2].GetComponent<Duckman>().player == player)
            {
                return true;
            }
        }

        return false;
    }

    public void RemovePiece(int x, int y)
    {
        GameObject piece = GetPosition(x, y);
        if (piece != null)
        {
            Destroy(piece);
            SetPositionEmpty(x, y);
        }
    }

    public void NextTurn()
    {
        currentPlayer = (currentPlayer == "A") ? "B" : "A";
    }

    void Update()
    {
        if (gameOver && Input.GetMouseButtonDown(0))
        {
            gameOver = false;
            SceneManager.LoadScene("Game");
        }
    }

    public string GetCurrentPlayer()
    {
        return currentPlayer;
    }

    public bool GameOver()
    {
        return gameOver;
    }
    public void InitiateMovePlates()
    {
     // placement phase
        for (int x = 0; x < 3; x++)
        { for (int y = 0; y < 3; y++)
            {if (GetPosition(x, y) == null)
                {SpawnMovePlate(x, y); }
            }
        }
    }

    public void SpawnMovePlate(int x, int y)
    {
        GameObject mp = Instantiate(movePlatePrefab, new Vector3(0, 0, -1), Quaternion.identity);
        MovePlate mpScript = mp.GetComponent<MovePlate>();
        mpScript.SetCoords(x, y);
        mpScript.SetReference(null); 
    }

}
