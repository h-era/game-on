using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlate : MonoBehaviour
{
    public GameObject controller;

//mover's initial pos
    GameObject reference = null ;

 //pos on board
    int xBoard ;
    int yBoard;

    //checking whether only movement or allignment and deletion 
    public bool AlignAndRemove = false ;

    //starter 
    public void Start()
    {
        controller = GameObject.FindGameObjectWithTag("GameController");

        // Change color if alignment and removal
        if (AlignAndRemove)
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
        }

        SetCoords(xBoard, yBoard);
    }

    public void SetReference(GameObject obj)
    {
        reference = obj;
    }

    public void SetCoords(int x, int y)
    {
        xBoard = x;
        yBoard = y;

        float xPos = x * 0.66f - 2.3f;
        float yPos = y * 0.66f - 2.3f;
        this.transform.position = new Vector3(xPos, yPos, -3.0f);
    }

    private void OnMouseUp()
    {
        Game game = controller.GetComponent<Game>();

        game.DestroyMovePlates();

        if (AlignAndRemove)
        {
            GameObject dp = game.GetPosition(xBoard, yBoard);
            Destroy(dp);
        }
        else
        {
            if (reference == null)
            {
                // Placement phase
                GameObject currentPlayerPiece = game.GetCurrentPlayer() == "A" ? game.playerA[game.placedPiecesA++] : game.playerB[game.placedPiecesB++];
                currentPlayerPiece.GetComponent<Duckman>().SetBoardPosition(xBoard, yBoard);
                currentPlayerPiece.GetComponent<Duckman>().SetCoords();
                game.SetPosition(currentPlayerPiece);
            }
            else
            {
                // Movement phase
                reference.GetComponent<Duckman>().SetBoardPosition(xBoard, yBoard);
                reference.GetComponent<Duckman>().SetCoords();
                game.SetPosition(reference);
            }

            game.CheckMill(xBoard, yBoard, reference == null ? game.GetCurrentPlayer() : reference.GetComponent<Duckman>().player);

            game.NextTurn();
            game.InitiateMovePlates();
        }
    }
}
