﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        //arrays to store ducks positions 
        int[] APositions;
        int[] BPositions;

        int placed = 0;

        char currentPlayer = 'A';

        //string imagePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images");
        string imagePath = System.Windows.Forms.Application.StartupPath + "\\";
        string soundPath = System.Windows.Forms.Application.StartupPath + "\\";



        //variables to store eliminated ducks 
        int ARemoved;
        int BRemoved;

        //checking whether allignment happened 
        bool AAligned = false;
        bool BAligned = false;

        bool isRemovingDuck = false;

        Button[] possiblePos;
        List<Button> ADucks;
        List<Button> BDucks;

        bool firstClick = true;
        Button clickedPos;

        private enum GameState
        {
            Placement,
            Movement,
            Removal,
            End
        }

        private GameState currentState = GameState.Placement;
        // Sound players for different actions
        private SoundPlayer placeSound;
        private SoundPlayer moveSound;
        private SoundPlayer removeSound;
        private SoundPlayer millSound;



        public Form1()
        {
            InitializeComponent();
            LoadSounds();
        }

        private void LoadSounds()
        {
            placeSound = new SoundPlayer(soundPath + "place.wav");
            moveSound = new SoundPlayer(soundPath + "move.wav");
            removeSound = new SoundPlayer(soundPath + "remove.wav");
            millSound = new SoundPlayer(soundPath + "mill.wav");
        }

        //
        public void moveDuck_Click(object sender, EventArgs e)
        {
            Button duck = sender as Button;
            Debug.WriteLine($"Current player: {currentPlayer}, Clicked position: {duck.Name}, Game state: {currentState}");

            switch (currentState)
            {
                case GameState.Placement:
                    if (duck.Image == null)
                    {
                        PlaceDuck(duck);
                        if (placed >= 18)
                        {
                            currentState = GameState.Movement;
                            Debug.WriteLine("Entering Movement phase");
                        }
                    }
                    break;

                case GameState.Movement:
                    HandleMovement(duck);
                    break;

                case GameState.Removal:
                    RemoveDuck(duck);
                    break;
            }

            UpdateMessage();
        }

        //placement phase 
        private void PlaceDuck(Button duck)
        {
            if (placed < 18)
            {
                if (currentPlayer == 'A')
                {
                    //duck.Image = Image.FromFile(Path.Combine(imagePath, "blue_duck.png"));
                    duck.Image = Image.FromFile(imagePath + "blue_duck.png");
                    duck.Tag = "APlayer";
                    ADucks.Add(duck);
                }
                else
                {
                    //duck.Image = Image.FromFile(Path.Combine(imagePath, "yellow_duck.png"));
                    duck.Image = Image.FromFile(imagePath + "yellow_duck.png");
                    duck.Tag = "BPlayer";
                    BDucks.Add(duck);
                }
                placed++;
                Debug.WriteLine($"Placed {currentPlayer} duck at {duck.Name}. Total placed: {placed}");
                CheckForMill(duck);
                placeSound.Play(); // Play place sound
            }
            Debug.WriteLine($"Placed {currentPlayer} duck at {duck.Name}. Total placed: {placed}");
            if (!CheckForMill(duck))
            {
                SwitchPlayer();
            }
        }

        private void HandleMovement(Button duck)
        {
            if (firstClick)
            {
                if ((currentPlayer == 'A' && ADucks.Contains(duck)) || (currentPlayer == 'B' && BDucks.Contains(duck)))
                {
                    clickedPos = duck;
                    firstClick = false;
                    Debug.WriteLine($"Selected {currentPlayer} duck at {duck.Name}");
                }
            }
            else
            {
                var test = IsValidMove(clickedPos, duck);
                if (duck.Image == null && IsValidMove(clickedPos, duck))
                {

                    MoveDuck(clickedPos, duck);
                    firstClick = true;
                    Debug.WriteLine($"Moved {currentPlayer} duck from {clickedPos.Name} to {duck.Name}");
                    moveSound.Play(); // Play move sound
                }
                else
                {
                    firstClick = true;
                    Debug.WriteLine($"Invalid move attempt from {clickedPos.Name} to {duck.Name}");
                }
            }
        }

        private void MoveDuck(Button initial, Button transfer)
        {
            string playerTag = initial.Tag.ToString();
            transfer.Image = initial.Image;
            transfer.Tag = playerTag;
            initial.Image = null;
            initial.Tag = null;

            if (currentPlayer == 'A')
            {
                ADucks[ADucks.IndexOf(initial)] = transfer;
            }
            else
            {
                BDucks[BDucks.IndexOf(initial)] = transfer;
            }

            Debug.WriteLine($"Moved {currentPlayer} duck from {initial.Name} to {transfer.Name}");


            if (!CheckForMill(transfer))
            {
                SwitchPlayer();
            }
        }

        private bool IsValidMove(Button from, Button to)
        {
            Dictionary<Button, Button[]> validMoves = new Dictionary<Button, Button[]>
            {
                { pos1, new Button[] { pos2, pos8} },
                { pos2, new Button[] { pos1, pos3, pos10 } },
                { pos3, new Button[] { pos2, pos4} },
                { pos4, new Button[] { pos3, pos5 ,pos12} },
                { pos5, new Button[] { pos4, pos6} },
                { pos6, new Button[] { pos5, pos14 ,pos7 } },
                { pos7, new Button[] { pos6, pos8 } },
                { pos8, new Button[] { pos1, pos7 ,pos16} },
                { pos9, new Button[] { pos10, pos16 } },
                { pos10, new Button[] { pos2, pos9, pos11 ,pos18 } },
                { pos11, new Button[] { pos10, pos12} },
                { pos12, new Button[] { pos4, pos11, pos13,pos20} },
                { pos13, new Button[] { pos12, pos14 } },
                { pos14, new Button[] { pos6, pos13, pos15, pos22} },
                { pos15, new Button[] { pos14, pos16 } },
                { pos16, new Button[] { pos8 ,pos9 ,pos15,pos24} },
                { pos17, new Button[] { pos18, pos24 } },
                { pos18, new Button[] { pos10, pos17 ,pos19 } },
                { pos19, new Button[] { pos18, pos20 } },
                { pos20, new Button[] { pos12, pos19, pos21 } },
                { pos21, new Button[] { pos20, pos22 } },
                { pos22, new Button[] { pos14,pos21, pos23 } },
                { pos23, new Button[] { pos22, pos24 } },
                { pos24, new Button[] { pos16,pos17, pos23 } },
            };

            return validMoves[from].Contains(to);
        }

        private bool CheckForMill(Button duck)
        {
            Button[] alignedPositions = CheckAlignment(duck);
            if (alignedPositions != null)
            {
                Debug.WriteLine($"Mill formed by {currentPlayer} at positions: {string.Join(", ", alignedPositions.Select(b => b.Name))}");
                foreach (var pos in alignedPositions)
                {
                    pos.BackColor = Color.Green;
                }
                SetupRemovalPhase();
                millSound.Play();
                return true;
            }
            return false;
        }

        private void RemoveDuck(Button duck)
        {
            if ((currentPlayer == 'A' && BDucks.Contains(duck)) || (currentPlayer == 'B' && ADucks.Contains(duck)))
            {
                duck.Image = null;
                duck.Tag = null;

                if (currentPlayer == 'A')
                {
                    BDucks.Remove(duck);
                    BRemoved++;
                }
                else
                {
                    ADucks.Remove(duck);
                    ARemoved++;
                }

                Debug.WriteLine($"Removed {(currentPlayer == 'A' ? 'B' : 'A')} duck from {duck.Name}");
                removeSound.Play(); // Play remove sound
                if (CheckGameOver())
                {
                    EndGame();
                }
                else
                {
                    ResetHighlights();
                    currentState = placed >= 18 ? GameState.Movement : GameState.Placement;
                    SwitchPlayer();
                }
            }
        }
        private bool CheckGameOver()
        {
            if (ARemoved >= 7 || BRemoved >= 7)
            {
                return true;
            }

            List<Button> playerDucks = (currentPlayer == 'A') ? ADucks : BDucks;
            return playerDucks.Count > 2 && !playerDucks.Any(duck => HasLegalMoves(duck));
        }

        private bool HasLegalMoves(Button duck)
        {
            return possiblePos.Any(pos => pos.Image == null && IsValidMove(duck, pos));
        }


        private void SetupRemovalPhase()
        {
            Debug.WriteLine($"Entering removal phase for player {currentPlayer}");
            currentState = GameState.Removal;
            UpdateMessage();

            // Highlight opponent's ducks that can be removed
            List<Button> opponentDucks = (currentPlayer == 'A') ? BDucks : ADucks;
            foreach (var duck in opponentDucks)
            {
                if (!IsPartOfMill(duck))
                {
                    duck.BackColor = Color.LightYellow;
                }
            }
        }

        private bool IsPartOfMill(Button duck)
        {
            return CheckAlignment(duck) != null;
        }


        private void SwitchPlayer()
        {
            currentPlayer = (currentPlayer == 'A') ? 'B' : 'A';
            Debug.WriteLine($"Switched to player {currentPlayer}");
        }

        private void ResetHighlights()
        {
            foreach (var pos in possiblePos)
            {
                pos.BackColor = SystemColors.Control;
            }
        }

        private void UpdateMessage()
        {

            string stateMessage = currentState == GameState.Removal ? "REMOVE AN OPPONENT'S DUCK" : "TURN";

            if (currentState == GameState.End)
            {
                stateMessage = "Wins! GAME OVER";
            }
            Message.Text = $"PLAYER {currentPlayer} {stateMessage}";
        }

        private Button[] CheckAlignment(Button duck)
        {
            // All possible mills
            Button[][] mills = new Button[][]
            {
                new Button[] { pos1, pos2, pos3 }, new Button[] { pos3, pos4, pos5 },
                new Button[] { pos5, pos6, pos7 }, new Button[] { pos7, pos8, pos1 },
                new Button[] { pos9, pos10, pos11 }, new Button[] { pos11, pos12, pos13 },
                new Button[] { pos13, pos14, pos15 }, new Button[] { pos15, pos16, pos9 },
                new Button[] { pos2, pos10, pos18 }, //new Button[] { pos4, pos12, pos8 },
                new Button[] { pos6, pos14, pos22 }, new Button[] { pos8, pos16, pos24 },
                new Button[] { pos17, pos18, pos19 }, new Button[] { pos19, pos20, pos21 },
                new Button[] { pos21, pos22, pos23 }, new Button[] { pos23, pos24, pos17 },
                new Button[] { pos20, pos12, pos4 }, //new Button[] { pos23, pos14,pos6 },
            };

            foreach (var mill in mills)
            {
                if (mill.Contains(duck))
                {
                    if (mill.All(pos => pos != null && pos.Tag != null && pos.Tag.ToString().Equals(duck.Tag.ToString())))
                    {
                        millSound.Play();
                        return mill;
                    }
                                      
                    
                }
            }
            return null;
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            // string AImagePath = imagePath + "blue_duck.png";
            //string BImagePath = imagePath + "yellow_duck.png";

            possiblePos = new Button[] { pos1 ,pos2 ,pos3 ,pos4, pos5, pos6, pos7 ,pos8,
                                         pos9 ,pos10 , pos11,pos12 ,pos13 ,pos14 ,pos15,
                                         pos16 ,pos17 ,pos18 ,pos19 ,pos20 ,pos21 ,pos22,
                                         pos23 , pos24};
            ADucks = new List<Button>(9);
            BDucks = new List<Button>(9);

            // ADucks = new Button[] {duck_A1,duck_A2,duck_A3,duck_A4,duck_A5,
            // duck_A6,duck_A7,duck_A8,duck_A9};

            // BDucks = new Button[] {duck_B1,duck_B2,duck_B3,duck_B4,duck_B5,
            // duck_B6,duck_B7,duck_B8,duck_B9 };

            resetgame();

        }

        private void resetgame()
        {
            AAligned = false;
            BAligned = false;

            ARemoved = 0;
            BRemoved = 0;
            placed = 0;

            currentState = GameState.Placement;
            currentPlayer = 'A';


            //removing objects 
            foreach (Button pos in possiblePos)
            {
                pos.Image = null;
                pos.Tag = null;
                pos.BackColor = SystemColors.Control;
            }

            ADucks.Clear();
            BDucks.Clear();

            //Initialising the positions outside the board
            APositions = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            BPositions = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            Debug.WriteLine("Game reset");
            UpdateMessage();
        }

        private void EndGame()
        {
            currentState = GameState.End;
            UpdateMessage();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void restart_Click(object sender, EventArgs e)
        {
            resetgame();
        }

        private void Start_Click(object sender, EventArgs e)
        {
            Message.Text = "PLAYER -" + currentPlayer + "TURN";
        }

        private void Message_Click(object sender, EventArgs e)
        {

        }
    }
}