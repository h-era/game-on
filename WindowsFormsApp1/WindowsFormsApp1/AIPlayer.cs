using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class AIPlayer
    {
        private char aiPlayer;
        private char humanPlayer;

        public AIPlayer(char aiPlayer, char humanPlayer)
        {
            this.aiPlayer = aiPlayer;
            this.humanPlayer = humanPlayer;
        }
    }
}
public Button GetBestMove(Button[] possiblePos, List<Button> aiDucks, List<Button> humanDucks, GameState currentState)
{
    int bestScore = int.MinValue;
    Button bestMove = null;

    foreach (var pos in possiblePos)
    {
        if (pos.Image == null)  // Check if the position is empty
        {
            pos.Tag = aiPlayer;
            aiDucks.Add(pos);

            int score = Minimax(possiblePos, aiDucks, humanDucks, false, 0, int.MinValue, int.MaxValue, currentState);

            pos.Tag = null;
            aiDucks.Remove(pos);

            if (score > bestScore)
            {
                bestScore = score;
                bestMove = pos;
            }
        }
    }

    return bestMove;
}

private int Minimax(Button[] possiblePos, List<Button> aiDucks, List<Button> humanDucks, bool isMaximizing, int depth, int alpha, int beta, GameState currentState)
{
    if (IsWinning(aiDucks))
    {
        return 10 - depth;
    }

    if (IsWinning(humanDucks))
    {
        return depth - 10;
    }

    if (IsDraw(possiblePos))
    {
        return 0;
    }

    if (isMaximizing)
    {
        int maxEval = int.MinValue;

        foreach (var pos in possiblePos)
        {
            if (pos.Image == null)  // Check if the position is empty
            {
                pos.Tag = aiPlayer;
                aiDucks.Add(pos);

                int eval = Minimax(possiblePos, aiDucks, humanDucks, false, depth + 1, alpha, beta, currentState);

                pos.Tag = null;
                aiDucks.Remove(pos);

                maxEval = Math.Max(maxEval, eval);
                alpha = Math.Max(alpha, eval);

                if (beta <= alpha)
                {
                    break;
                }
            }
        }

        return maxEval;
    }
    else
    {
        int minEval = int.MaxValue;

        foreach (var pos in possiblePos)
        {
            if (pos.Image == null)  // Check if the position is empty
            {
                pos.Tag = humanPlayer;
                humanDucks.Add(pos);

                int eval = Minimax(possiblePos, aiDucks, humanDucks, true, depth + 1, alpha, beta, currentState);

                pos.Tag = null;
                humanDucks.Remove(pos);

                minEval = Math.Min(minEval, eval);
                beta = Math.Min(beta, eval);

                if (beta <= alpha)
                {
                    break;
                }
            }
        }

        return minEval;
    }
}
private bool IsWinning(List<Button> ducks)
{
    // Implement the logic to check if the given list of ducks forms a winning configuration
    // Check for all possible mills
    return false;  // Placeholder
}

private bool IsDraw(Button[] possiblePos)
{
    // Check if all positions are filled (i.e., a draw)
    return possiblePos.All(pos => pos.Image != null);
}

